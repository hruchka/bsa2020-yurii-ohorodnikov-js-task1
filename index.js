const API_URL = 'https://api.github.com/repositories/186232982/contents/resources/api/';
const rootElement = document.getElementById('root');
const loadingElement = document.getElementById('loading-overlay');
const fightersDetailsMap = new Map();

    function callApi(endpoind, method) {
    const url = API_URL + endpoind
    const options = {
      method
    };
  
    return fetch(url, options)
      .then(response => 
        response.ok 
          ? response.json() 
          : Promise.reject(Error('Failed to load'))
      )
      .catch(error => { throw error });
  }

  class Fighter{
    constructor(id, name, health, attack, defense){
        this.id = id;
        this.name = name;
        this.health = health;
        this.attack = attack;
        this.defense = defense;
    }

    getHitPower(){
        const criticalHitChance = Math.floor(Math.random() * 2) + 1;
        const power = this.attack * criticalHitChance;
        return power;
    }

    getBlockPower(){
        const dodgeChance = Math.floor(Math.random() * 2) + 1;
        const power = this.defense * dodgeChance;
        return power;
    }
}

  class FighterService {
    async getFighters() {
      try {
        const endpoint = 'fighters.json';
        const apiResult = await callApi(endpoint, 'GET');
  
        return JSON.parse(atob(apiResult.content));
      } catch (error) {
        throw error;
      }
    }
    async getFighterDetails(id){
        try{
            const endpoint = `details/fighter/${id}.json`;
            const apiResult = await callApi(endpoint, 'GET');

            return JSON.parse(atob(apiResult.content));
        }
        catch(error){
            throw error;
        }
    }
}
  
  

  class View {
    element;
  
    createElement({ tagName, className = '', attributes = {} }) {
      const element = document.createElement(tagName);
      element.classList.add(className);
      
      Object.keys(attributes).forEach(key => element.setAttribute(key, attributes[key]));
  
      return element;
    }
  }

  class FighterView extends View {
    constructor(fighter, handleClick) {
      super();
  
      this.createFighter(fighter, handleClick);
    }
  
    createFighter(fighter, handleClick) {
      const { name, source } = fighter;
      const nameElement = this.createName(name);
      const imageElement = this.createImage(source);
  
      this.element = this.createElement({ tagName: 'div', className: 'fighter' });
      this.element.append(imageElement, nameElement);
      this.element.addEventListener('click', event => handleClick(event, fighter), false);
    }
  
    createName(name) {
      const nameElement = this.createElement({ tagName: 'span', className: 'name' });
      nameElement.innerText = name;
  
      return nameElement;
    }
  
    createImage(source) {
      const attributes = { src: source };
      const imgElement = this.createElement({
        tagName: 'img',
        className: 'fighter-image',
        attributes
      });
  
      return imgElement;
    }
  }
  
  class FightersView extends View {
    constructor(fighters) {
      super();
      this.fighterService = new FighterService();
      this.handleClick = this.handleFighterClick.bind(this);
      this.createFighters(fighters);
    }
  
    fightersDetailsMap = new Map();
  
    createFighters(fighters) {
      const fighterElements = fighters.map(fighter => {
        const fighterView = new FighterView(fighter, this.handleClick);
        return fighterView.element;
      });
  
      this.element = this.createElement({ tagName: 'div', className: 'fighters' });
      this.element.append(...fighterElements);
    }
    
    

    async handleFighterClick(event, fighter) {
       try {
        const resFighter = await this.fighterService.getFighterDetails(fighter._id)
        console.log(resFighter)
        const {_id, name, health, attack, defense} = resFighter;
        const currentFighter = new Fighter(_id, name, health, attack, defense);
        console.log("Hit Power: ", currentFighter.getHitPower());
        console.log("Block Power: ", currentFighter.getBlockPower()); 
       } catch (error) {
           console.log(error)
       } 
    }
  }
  

  